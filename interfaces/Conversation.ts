import Message from './Message';
interface Conversation{
    buyerId:string;
    sellerId:string;
    id:string;
    messages:Message[];
    sellerName:string;
    buyerName:string;
}
export default Conversation;
