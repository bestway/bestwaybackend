// tslint:disable-next-line:interface-name
interface User {
    id:string;
    firstName:string;
    lastName:string;
    school:string;
    email:string;
    phoneNumber:string;
}
export default User;
