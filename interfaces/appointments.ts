interface Appointment {

    id: string;
    buyerId:string;
    sellerId:string;
    buyerName:string;
    sellerName:string;
    location:string;
    time:string;
    approved:boolean;

}

export default Appointment;