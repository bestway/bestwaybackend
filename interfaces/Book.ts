
// tslint:disable-next-line:interface-name
interface Book {

    id: string,
    title:string ;
    description:string;
    quality:number;
    isbn10:string;
    isbn13:string;
    retailPrice:string;
    year:number;
    edition:string;
    publisher:string;
    author:string;
    userEmail:string;
}
export default Book;
