import { Application } from 'express';
import examplesRouter from './api/controllers/examples/router'
import userRouter from './api/controllers/user/router'
import bookRouter from './api/controllers/books/router'
import conversationRouter from './api/controllers/conversation/router'
export default function routes(app: Application): void {
  app.use('/api/v1/examples', examplesRouter);
  app.use('/api/v1/users', userRouter);
  app.use('/api/v1/books', bookRouter);
  app.use('/api/v1/conversation', conversationRouter);

};