import { N1qlQuery, Bucket } from "couchbase";
import User from "../../../interfaces/User";
import { Result } from "range-parser";
import bucket from "./dbConnection";

class UserDb {
  
  private bucket: Bucket = null;
  constructor() {
    this.bucket = bucket;
  }

  async createUser(userDoc: User) {
    let result;
    await this.bucket.insert(userDoc.id, userDoc, async (err, result) => {
      if (err) {
        let result = null;
      }
      await this.bucket.get(userDoc.id, (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result.value);
        }
      });
    });
  }
  async getUserById(id: string) {
    let userResponse:User;
    await this.bucket.get(id, async (err, result) => {
      if (err) {
        console.log(err);
        userResponse = null;
      } else {
        userResponse = result.value as User;
      }
    });
    return userResponse;
  }
}
export default UserDb;
