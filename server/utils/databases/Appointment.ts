import { N1qlQuery, Bucket } from "couchbase";
import { Result } from "range-parser";
import bucket from "./dbConnection";
import Appointment from "../../../interfaces/appointments";
class AppointmentDbHandler {

    private bucket: Bucket = null;

    constructor() {
        this.bucket = bucket;
    }

    createAppointment(AppointmentDoc: Appointment) {

        this.bucket.insert(AppointmentDoc.id, AppointmentDoc, (err, result) => {
          if (err) {
            return err;
          }
        });
    }


}
export default AppointmentDbHandler;