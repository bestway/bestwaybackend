 /**
   * couchbase db connection
   */
import { Cluster, N1qlQuery, Bucket } from "couchbase";
import config from "../../../utils/passportConfig";
const cluster = new Cluster(config.couchbase.server);
cluster.authenticate("Administrator", config.couchbase.password);
const bucket: Bucket = cluster.openBucket(config.couchbase.bucket);
export default bucket;
