import { N1qlQuery } from "couchbase";
import Book from "../../../interfaces/Book";
import bucket from "./dbConnection";
import Bluebird from 'bluebird';
import { BooksDbHandlerDelegate } from "../../api/services/BooksDnHandelerDelegate";

class BooksDbHandler implements BooksDbHandlerDelegate {
    
    static sharedInstance = new BooksDbHandler();

    private constructor() {}

    createBook(bookDoc: Book) {

        bucket.insert(bookDoc.id, bookDoc, (err, result) => {
          if (err) {
            return err;
          }
          return bucket.get(bookDoc.id, (err, result) => {
            if (err) {
              console.log(err);
            } else {
              console.log(result.value);
            }
          });
        });
    }

    getBooksByParam(param: string): Bluebird<Book[]> {

      return new Bluebird(function(resolve, reject) {

          bucket.query(
              N1qlQuery.fromString(`SELECT * FROM BestWay WHERE 
                                    title LIKE "%${param}%" or 
                                    isbn10 LIKE "%${param}%" or 
                                    isbn13 LIKE "%${param}%"`),
              // tslint:disable-next-line:typedef
              function(err, rows) {
                if (err) {
                  reject(err);
                } else {
                  
                  var results: Book[] = new Array();

                  rows.forEach(row => {
                      
                      results.push(row.BestWay);
                  });

                  resolve(results);
                }
              }
          );
      });
    }

    getBooksByEmail(email: string): Bluebird<Book[]> {

        return new Bluebird(function(resolve, reject) {

            bucket.query(
                N1qlQuery.fromString(`SELECT * FROM BestWay WHERE userEmail="${email}"`),

                function(err, rows) {
                  if (err) {
                    reject(err);
                  } else {
                    
                    var results: Book[] = new Array();

                    rows.forEach(row => {
                        
                        results.push(row.BestWay);
                    });

                    resolve(results);
                  }
                }
              );
        });
    }
}

export default BooksDbHandler;
