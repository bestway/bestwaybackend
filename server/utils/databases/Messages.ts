 /**
   *message mode db util
   */
import User from "../../../interfaces/User";
import Conversation from "./../../../interfaces/Conversation";
import { Cluster, N1qlQuery, Bucket } from "couchbase";
import { Result } from "range-parser";
import Message from "../../../interfaces/Message";
import bucket from "./dbConnection";
import Book from '../../../interfaces/Book';
import eventToPromise = require("event-to-promise");
import L from "../../common/logger";

class Messagedb {
  private bucket: Bucket = null;
  constructor() {
    this.bucket = bucket;
  }

  createConversation(conversationDoc: Conversation) {
    
    this.bucket.insert(conversationDoc.id, conversationDoc, (err, result) => {
      if (err) {
        return err;
      } else {
        this.bucket.get(conversationDoc.id, (err, result) => {
          if (err) {
            console.log('Error: ', err);
          } else {
            console.log('Value: ', result.value);
          }
        });
      }
    });
  }

  getAllConversations():Promise<Conversation[]> {

    return new Promise(function(resolve, reject) {
        bucket.query(
            N1qlQuery.fromString(`SELECT * FROM BestWay WHERE type="conversation"`),
            function(err, rows) {
              if (err) {
                reject(err);
              } else {
                let results: Conversation[] = new Array();
L.debug("rows",JSON.stringify(rows));
                 rows.forEach(row => {
                    results.push(row.BestWay as Conversation);
                });
                resolve(results);
              }
            }
          );
    });
  }

  getConversationById(id: string): Promise<Conversation> {
    return new Promise(function(resolve, reject) {

      bucket.get(id, (err, result) => {

        if (err) {

          reject(err);
        }
        else {

          resolve(result.value as Conversation);
        }
      });
      }
    );
  }

  getMessagesByConversationId(id: string): Promise<Message[]> {
    console.log(id)

    return new Promise(function(resolve, reject) {
console.log(id)
      bucket.get(id, (err, result) => {

        if (err) {

          reject(err);
        }
        else {

          resolve(result.value.messages);
        }
      });
      }
    );
    }

  CreateMessage(message: Message) {
    let status: string;
    this.bucket.insert(message.id, message, (err, result) => {
      if (err) {
        Promise.resolve("couldn't insert:" + err).then(value => {
          status = value;
        });
      } else {
        Promise.resolve("inserted" + result.value).then(value => {
          status = value;
        });
      }
    });
    console.log(status)
    return status;
  }
  getConversationsByUserId(id: string):Promise<Conversation[]> {
    return new Promise(function(resolve, reject) {

      bucket.query(
          N1qlQuery.fromString(`SELECT * FROM BestWay WHERE buyerId="${id}" and type="conversation"`),
          function(err, rows) {
            if (err) {
              reject(err);
            } else {
              let results: Conversation[] = new Array();
               rows.forEach(row => {
                  results.push(row.BestWay as Conversation);
              });
              resolve(results);
            }
          }
        );
  });
}
}

export default Messagedb;