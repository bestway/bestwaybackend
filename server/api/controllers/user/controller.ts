import UserService from '../../services/user.service';
import { Request, Response } from 'express';

export class UsersController {
   /**
   * Gets all users
   */
  all(req: Request, res: Response): void {
    UserService.all().then(r => res.json(r));
  }
 /**
   * Gets all users with the appropriate credentials
   */
  byCredentials(req: Request, res: Response): void {
    UserService.byCredentials(req.body.email, req.body.password).then(r => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }
}
export default new UsersController();
