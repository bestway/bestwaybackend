import express from 'express';
import controller from './controller'
export default express.Router()
    .post('/', controller.uploadBook)
    .get('/:email', controller.byEmail)
    .get('/search/:param', controller.byParam);
