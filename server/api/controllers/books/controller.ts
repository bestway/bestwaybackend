import BookService from '../../services/book.service';
import { Request, Response } from 'express';

export class BooksController {
  
  /**
   * Retrieves books by email for specific user 
   * @param req 
   * @param res 
   */
  byEmail(req: Request, res: Response): void {
    BookService.byEmail(req.params.email).then(r => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  /**
   * Retrieves books by param when user searches the book
   * @param req 
   * @param res 
   */
  byParam(req: Request, res: Response): void {
    BookService.byParam(req.params.param).then(r => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  /**
   * Uploads a new book to the server
   * @param req 
   * @param res 
   */
  uploadBook(req: Request, res: Response): void {
    BookService.uploadBook(req.body.book_object).then(r =>
      res
        .status(201)
        .location(`/api/v1/books/${r.userEmail}`)
        .json(r),
    );
  }
}
export default new BooksController();
