import ConversationService from "../../services/conversation.service";
import { Request, Response } from "express";
import Message from "../../../../interfaces/Message";
import { Socket } from 'socket.io';

export class Controller {
  getAllConversationByUserId(req: Request, res: Response): any {
    return ConversationService.getAllConversationByUserId(
      req.params.userId
    ).then(r => res.json(r));
  }
  /**
   * Gets all conversations
   * @param req
   * @param res
   */
  allMessagesById(req: Request, res: Response): void {
    ConversationService.allMessagesByConversationId(
      req.params.conversationId
    ).then(r => res.json(r));
  }

  /**
   * Gets a conversation by id
   * @param req
   * @param res
   */
  byId(req: Request, res: Response): void {
    ConversationService.byId(req.params.id).then(r => {
      if (r) {
        res.json(r);
      } else {
        res.status(404).end();
      }
    });
  }

  /**
   * Creates a new conversation
   * @param req
   * @param res
   */
  create(req: Request, res: Response): void {
    ConversationService.create(req.body.conversation_object).then(r =>
      res
        .status(201)
        .location(`/api/v1/conversation/${r.id}`)
        .json(r)
    );
  }

  createMessage(message_object:Message​​,socket:Socket) {
 ConversationService.createMessage(message_object);
      socket.emit("newMessageCreated",message_object);
  }
}
export default new Controller();
