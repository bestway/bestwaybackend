import express from 'express';
import controller from './controller'
export default express.Router()
    .get('/messages/:conversationId', controller.allMessagesById)
    .get('/:userId', controller.getAllConversationByUserId)
    .get('/:id', controller.byId)
    .post('/', controller.create);  

