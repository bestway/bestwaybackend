import AppointmentService from "../../services/appointment.service";
import { Request, Response } from "express";
import Appointment from "../../../../interfaces/appointments";
import { Socket } from 'socket.io';

export class Controller {

//create a new appointment
  createAppointment(appointment_object:Appointment,socket:Socket) {
 AppointmentService.createAppointment(appointment_object);
 socket.emit("newPendingAppointment",appointment_object);
  }

}
export default new Controller();
