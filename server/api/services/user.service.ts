import Promise from 'bluebird';
import L from '../../common/logger'

interface User {

  email: string,
  password: string
};

const users: User[] = [
    { email: "test", password: '1234' }, 
    { email: "test2", password: '1234' }
];

export class UserService {

  all(): Promise<User[]> {
    L.info(users, 'fetch all users');
    return Promise.resolve(users);
  }

  byCredentials(email: string, password: string): Promise<User> {
    L.info(`fetch user with email ${email}`);
    return this.all().then(r => r.find(item => item.email == email && item.password == password))
  }
}

export default new UserService();