import Promise from "bluebird";
import AppointmentDbHandler from "./../../utils/databases/Appointment";
import L from "../../common/logger";
import * as uid from 'uuid';
import Appointment from '../../../interfaces/appointments';

export class AppointmentService {

  appointmentbUtil = new AppointmentDbHandler();

  createAppointment(appointment:Appointment):void {
    this.appointmentbUtil.createAppointment(appointment);
}
}

export default new AppointmentService();