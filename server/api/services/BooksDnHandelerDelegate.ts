import Promise from 'bluebird';
import Book from '../../../interfaces/Book';

export interface BooksDbHandlerDelegate {

    getBooksByEmail(email: string): Promise<Book[]>;
    getBooksByParam(param: string): Promise<Book[]>;
    createBook(bookDoc: Book);
}