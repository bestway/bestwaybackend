import Promise from "bluebird";
import  Conversation  from "./../../../interfaces/Conversation";
import L from "../../common/logger";
import Message from "../../../interfaces/Message";
import Messagedb from "../../utils/databases/Messages";
import * as uid from 'uuid';

export class ConversationService {
 
  messagedbUtil = new Messagedb();

 /**
   * Gets conversation by id
   */
  byId(id: string): Promise<Conversation> {
    const converstaionsByid=this.messagedbUtil.getAllConversations().then((conversations)=>{
      return conversations.filter((chat)=>
        (chat.id===id)
      );
    });
    return Promise.resolve(converstaionsByid[0]);
  }

 /**
   * create a new conversation
   */
  create(conversation: Conversation): Promise<Conversation> {
   
    conversation['id'] = uid.v4()
    this.messagedbUtil.createConversation(conversation);

    return Promise.resolve(conversation);
}
 /**
   *get all messages from a conversation
   */
allMessagesByConversationId(id:string): Promise<Message[]> {

   return Promise.resolve(this.messagedbUtil.getMessagesByConversationId(id));
  }

  getAllConversationByUserId(id:string): Promise<Conversation[]> {
    const converstaionsByUSer=this.messagedbUtil.getAllConversations().then((conversations)=>{
      return conversations.filter((chat)=>
        (chat.buyerId===id||chat.sellerId===id)
      );
    });

    return Promise.resolve(converstaionsByUSer);
   }

 /**
   * create a new message
   */
  createMessage(message:Message):void {
    this.messagedbUtil.CreateMessage(message);
}
}

export default new ConversationService();