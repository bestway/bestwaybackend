import Promise from 'bluebird';
import L from '../../common/logger'
import Book from '../../../interfaces/Book';
import BooksDbHandler from '../../utils/databases/BooksDbHandler';
import * as uid from 'uuid';
import { BooksDbHandlerDelegate } from './BooksDnHandelerDelegate'

export class BookService {

    private booksDbHandlerDelegate: BooksDbHandlerDelegate = null;

    constructor() {

        this.booksDbHandlerDelegate = BooksDbHandler.sharedInstance;
    }

    /**
     * Get book by email
     */
    byEmail(userEmail: string): Promise<Book[]> {
        L.info(`fetch books with id ${userEmail}`);
        return Promise.resolve(this.booksDbHandlerDelegate.getBooksByEmail(userEmail));
    }

    /**
     * Get book by param
     */
    byParam(param: string): Promise<Book[]> {
        L.info(`fetch all books with id ${param}`);
        return Promise.resolve(this.booksDbHandlerDelegate.getBooksByParam(param));
    }

    /**
     * Upload a new book
     * @param book 
     */
    uploadBook(book: Book): Promise<Book> {
        L.info(`create example with name ${book}`);

        // TODO: check if exists
        book['id'] = uid.v4();//String(Math.floor(100000 + Math.random() * 900000));
        this.booksDbHandlerDelegate.createBook(book);
        return Promise.resolve(book);
      }
}

export default new BookService();