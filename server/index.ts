import "./common/env";
import Server from "./common/server";
import routes from "./routes";
import { Socket } from "socket.io";
import Message from "../interfaces/Message";
import Appointment from "../interfaces/appointments";
import { Controller } from "./api/controllers/conversation/controller";
import { Controller as AppointmentController } from "./api/controllers/appointments/controller";
import uuid = require("uuid");
const socket: Socket = require("socket.io").listen(80);
/**
 * entry point of the project/server
 */
// tslint:disable-next-line:radix
const port = parseInt(process.env.PORT);

socket.on("connection", function(client: Socket) {
  // socket for message creation
  client.on("newMessage", (newMessage: Message) => {
    let messageObject = newMessage;
    messageObject.dateCreated = new Date().toDateString();
    messageObject.id = uuid.v1();
    console.log(messageObject);

    let controller: Controller = new Controller();
    controller.createMessage(messageObject as Message, client);
  });
  //handles appointment
  // this is not using the router because in the future the appointment should notify both parties in real time
  client.on("newAppointment", newAppointment => {
    let messageObject: Appointment;
    messageObject.approved = false;
    messageObject.id = uuid.v4();
    let controller: AppointmentController = new AppointmentController();
    controller.createAppointment(newAppointment as Appointment, client);
  });
  client.on("disconnect", onDisconnect);
  function onDisconnect() {
    console.log("Received: disconnect event from client: " + client.id);
    client.removeListener("disconnect", onDisconnect);
  }
});

export default new Server().router(routes).listen(port);
